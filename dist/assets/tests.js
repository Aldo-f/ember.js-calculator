'use strict';

define("calculator/tests/integration/components/display-sum-test", ["qunit", "@ember/test-helpers", "ember-qunit"], function (_qunit, _testHelpers, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Integration | Component | displaySum', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        <DisplaySum />
      */
      {
        id: "LEonX6zs",
        block: "{\"symbols\":[],\"statements\":[[5,\"display-sum\",[],[[],[]]]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        
            <DisplaySum>
              template block text
            </DisplaySum>
          
      */
      {
        id: "N53Sje3q",
        block: "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"display-sum\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("calculator/tests/integration/components/input-number-test", ["qunit", "@ember/test-helpers", "ember-qunit"], function (_qunit, _testHelpers, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Integration | Component | inputNumber', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        <InputNumber />
      */
      {
        id: "VqlHX7Vg",
        block: "{\"symbols\":[],\"statements\":[[5,\"input-number\",[],[[],[]]]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        
            <InputNumber>
              template block text
            </InputNumber>
          
      */
      {
        id: "ZhY8dpNL",
        block: "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"input-number\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("calculator/tests/integration/components/list-numbers-test", ["qunit", "@ember/test-helpers", "ember-qunit"], function (_qunit, _testHelpers, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Integration | Component | listNumbers', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        <ListNumbers />
      */
      {
        id: "Ec+Rs6tV",
        block: "{\"symbols\":[],\"statements\":[[5,\"list-numbers\",[],[[],[]]]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        
            <ListNumbers>
              template block text
            </ListNumbers>
          
      */
      {
        id: "Z+OQKdmk",
        block: "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"list-numbers\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("calculator/tests/integration/components/overview-test", ["qunit", "@ember/test-helpers", "ember-qunit"], function (_qunit, _testHelpers, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Integration | Component | overview', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);
    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });
      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        <Overview />
      */
      {
        id: "t8Q3sOZK",
        block: "{\"symbols\":[],\"statements\":[[5,\"overview\",[],[[],[]]]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), ''); // Template block usage:

      await (0, _testHelpers.render)(Ember.HTMLBars.template(
      /*
        
            <Overview>
              template block text
            </Overview>
          
      */
      {
        id: "vFfnmz13",
        block: "{\"symbols\":[],\"statements\":[[0,\"\\n      \"],[5,\"overview\",[],[[],[]],{\"statements\":[[0,\"\\n        template block text\\n      \"]],\"parameters\":[]}],[0,\"\\n    \"]],\"hasEval\":false}",
        meta: {}
      }));
      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define("calculator/tests/lint/app.lint-test", [], function () {
  "use strict";

  QUnit.module('ESLint | app');
  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });
  QUnit.test('components/display-sum.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/display-sum.js should pass ESLint\n\n');
  });
  QUnit.test('components/input-number.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/input-number.js should pass ESLint\n\n');
  });
  QUnit.test('components/list-numbers.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/list-numbers.js should pass ESLint\n\n');
  });
  QUnit.test('components/overview.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/overview.js should pass ESLint\n\n');
  });
  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });
  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });
  QUnit.test('routes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass ESLint\n\n');
  });
});
define("calculator/tests/lint/templates.template.lint-test", [], function () {
  "use strict";

  QUnit.module('TemplateLint');
  QUnit.test('calculator/templates/application.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'calculator/templates/application.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('calculator/templates/components/display-sum.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'calculator/templates/components/display-sum.hbs should pass TemplateLint.\n\ncalculator/templates/components/display-sum.hbs\n  1:38  error  you must use double quotes in templates  quotes\n');
  });
  QUnit.test('calculator/templates/components/input-number.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'calculator/templates/components/input-number.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('calculator/templates/components/list-numbers.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'calculator/templates/components/list-numbers.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('calculator/templates/components/overview.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'calculator/templates/components/overview.hbs should pass TemplateLint.\n\n');
  });
  QUnit.test('calculator/templates/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'calculator/templates/index.hbs should pass TemplateLint.\n\n');
  });
});
define("calculator/tests/lint/tests.lint-test", [], function () {
  "use strict";

  QUnit.module('ESLint | tests');
  QUnit.test('integration/components/display-sum-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/display-sum-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/input-number-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/input-number-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/list-numbers-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/list-numbers-test.js should pass ESLint\n\n');
  });
  QUnit.test('integration/components/overview-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/overview-test.js should pass ESLint\n\n');
  });
  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });
  QUnit.test('unit/routes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/index-test.js should pass ESLint\n\n');
  });
});
define("calculator/tests/test-helper", ["calculator/app", "calculator/config/environment", "@ember/test-helpers", "ember-qunit"], function (_app, _environment, _testHelpers, _emberQunit) {
  "use strict";

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));
  (0, _emberQunit.start)();
});
define("calculator/tests/unit/routes/index-test", ["qunit", "ember-qunit"], function (_qunit, _emberQunit) {
  "use strict";

  (0, _qunit.module)('Unit | Route | index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);
    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:index');
      assert.ok(route);
    });
  });
});
define('calculator/config/environment', [], function() {
  var prefix = 'calculator';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('calculator/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
