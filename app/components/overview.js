import {
  A
} from '@ember/array';
import Component from '@ember/component';
import {
  computed
} from '@ember/object';

export default Component.extend({
  numberList: null,
  title: "Calculator",

  init() {
    this._super(...arguments);
    this.set('numberList', A([{
      number: 50
    }, {
      number: 70
    }, {
      number: 80
    }, {
      number: 17
    }]));
  },
  actions: {
    addNumber(number) {
      if (!isNaN(parseFloat(number))) {
        this.numberList.pushObject({
          number: parseFloat(number)
        });
      }
    },
    deleteNumber(number) {
      this.numberList.removeObject(number)
    },
    reset() {
      this.set('numberList', [])
    }
  },
  display: computed('numberList.[]', function () {
    let sum = 0;
    this.numberList.forEach(obj => {
      sum += Object.keys(obj).reduce((sum, key) => sum + parseFloat(obj[key] || 0), 0);
    });

    let even = sum != 0 && sum % 2 == 0 ? true : false

    if (sum % 1) {
      sum = +(Math.round(sum + 'e+2') + 'e-2');
    }

    let display = {
      sum: sum,
      even: even
    }

    return display;
  }),
});
